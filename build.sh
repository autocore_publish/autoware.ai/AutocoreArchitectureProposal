

if [ $# != 1 ]; then

	echo "usage: ./build.sh [rootpasswd]";
	return
else 
	passwd=$1	
fi
source /opt/ros/melodic/setup.bash
current_path=${PWD}

osqp_path=${current_path}/ansible/roles/osqp/osqp
cd ${osqp_path}
echo  ${osqp_path}

if [ ! -d "build" ];then 
	mkdir build
	cd build/
	cmake -G "Unix Makefiles" .. chdir=../../..
	make all 
	echo $1 | sudo -S make install
	 
fi

cd ${current_path}

catkin build --cmake-args -DCMAKE_BUILD_TYPE=Release
