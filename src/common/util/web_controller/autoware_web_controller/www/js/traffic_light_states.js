if (!TrafficLightStatesGreenPublisher) {
    var TrafficLightStatesGreenPublisher = {
        ros: null,
        name: "",
        init: function() {
            this.ros = new ROSLIB.Ros();
            this.ros.on('error', function(error) {
                document.getElementById('traffic_light_states').innerHTML = "Error";
            });
            this.ros.on('connection', function(error) {
                document.getElementById('traffic_light_states').innerHTML = "Connected";
            });
            this.ros.on('close', function(error) {
                document.getElementById('traffic_light_states').innerHTML = "Closed";
            });
            this.ros.connect('ws://' + location.hostname + ':9090');
        },
        send: function() {
            var pub = new ROSLIB.Topic({
                ros: this.ros,
                name: '/web_controller/manual_overide_traffic_light_colour',
                messageType: 'std_msgs/Bool',
                latch: 'true'
            });

            var str = new ROSLIB.Message({
                data: true
            });
            pub.publish(str);
        }
    }
    TrafficLightStatesGreenPublisher.init();

    window.onload = function() {};
    window.onunload = function() {
        TrafficLightStatesGreenPublisher.ros.close();
    };
}
if (!TrafficLightStatesRedPublisher) {
    var TrafficLightStatesRedPublisher = {
        ros: null,
        name: "",
        init: function() {
            this.ros = new ROSLIB.Ros();
            this.ros.on('error', function(error) {
                document.getElementById('traffic_light_states').innerHTML = "Error";
            });
            this.ros.on('connection', function(error) {
                document.getElementById('traffic_light_states').innerHTML = "Connected";
            });
            this.ros.on('close', function(error) {
                document.getElementById('traffic_light_states').innerHTML = "Closed";
            });
            this.ros.connect('ws://' + location.hostname + ':9090');
        },
        send: function() {
            var pub = new ROSLIB.Topic({
                ros: this.ros,
                name: '/web_controller/manual_overide_traffic_light_colour',
                messageType: 'std_msgs/Bool',
                latch: 'true'
            });

            var str = new ROSLIB.Message({
                data: false
            });
            pub.publish(str);
        }
    }
    TrafficLightStatesRedPublisher.init();

    window.onload = function() {};
    window.onunload = function() {
        TrafficLightStatesRedPublisher.ros.close();
    };
}
if (!TrafficLightStatesSubscriber) {
    var TrafficLightStatesSubscriber = {
        ros: null,
        name: "",
        init: function() {
            this.ros = new ROSLIB.Ros();
            this.ros.on('error', function(error) {
                document.getElementById('state').innerHTML = "Error";
            });
            this.ros.on('connection', function(error) {
                document.getElementById('state').innerHTML = "Connect";
            });
            this.ros.on('close', function(error) {
                document.getElementById('state').innerHTML = "Close";
            });
            this.ros.connect('ws://' + location.hostname + ':9090');

            var sub = new ROSLIB.Topic({
                ros: this.ros,
                name: '/web_controller/manual_overide_traffic_light_colour',
                messageType: 'std_msgs/Bool'
            });
            sub.subscribe(function(message) {
                const div = document.getElementById("traffic_light_colour");
                if (div.hasChildNodes()) {
                    div.removeChild(div.firstChild);
                }
                // var res = message.data;
                var res;
                if (message.data)
                {
                    res = "GREEN";
                }else
                {
                    res = "RED";
                }
                var el = document.createElement("span");
                el.innerHTML = res
                document.getElementById("traffic_light_colour").appendChild(el);
            });
        }
    }
    TrafficLightStatesSubscriber.init();

    window.onload = function() {};
    window.onunload = function() {
        TrafficLightStatesSubscriber.ros.close();
    };
}
